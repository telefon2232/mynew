# Unique chat-bot vk
### This bot has many commands:
* Лайк на аву - Bot likes your avatar.
* Оцени [...] - Bot evaluates the word.
* Когда [фраза] - When will be event?
* Кто [описание] - Who is it?
* Помощь - Help.
* Инфа [фраза] - Bot writes your chance in percent.
* Коммент на аву - Bot comments your avatar.
* Новости - Bot sends to you actual news.
* Статус - Bot's status.
* Про меня - Bot writes information about you.
* Двач - Bot writes random post from 2ch.hk.
* Купоны - Bot parses and  writes kupons KFS.
* Погода [город] - Bot writes the weather in your city.
* Аккорды - parses site with chords.
* Скажи [текст] - Bot sends voice message with your text.

# Dependencies:
`pip3 install vk`